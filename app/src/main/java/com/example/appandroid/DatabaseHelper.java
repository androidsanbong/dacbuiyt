package com.example.appandroid;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DatabaseHelper extends SQLiteOpenHelper{

    public DatabaseHelper(Context context) {
        super(context, "Login.db",null,1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(" Create table user(uname text primary key,password text,email text)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists user");
    }
    //insertting in database
    public Boolean insert(String uname, String password){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("uname",uname);
        contentValues.put("password",password);

        long ins = db.insert("user",null,contentValues);
        if (ins == -1)
            return false;
        else
            return true;
    }

    //checking if username exists;
    public Boolean checkuname(String uname){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("Select * from user where uname=?",new String[]{uname});
        if (cursor.getCount()>0)
            return false;
        else
            return true;
    }
    //checking the uname and password;
    public Boolean unamepassword(String uname,String password){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from user where uname=? and password=?",new String[]{uname,password});
        if(cursor.getCount()>0)
            return true;
        else
            return false;
    }
}
