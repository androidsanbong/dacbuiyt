package com.example.appandroid;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class SanBongAdapter extends BaseAdapter {


    Context myContext;
    int myLayout;
    List<SanBong> arraySanBong;

    public SanBongAdapter(Context context, int layout,List<SanBong> sanBongList){
        myContext = context;
        myLayout = layout;
        arraySanBong = sanBongList;
    }

    @Override
    public int getCount() {
        return arraySanBong.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) myContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        convertView = inflater.inflate(myLayout,null);

        //ánh xạ và gán giá trị

        TextView txtTen = (TextView) convertView.findViewById(R.id.txtten);
        txtTen.setText(arraySanBong.get(position).Ten);
        TextView txtdc = (TextView) convertView.findViewById(R.id.txtdc);
        txtdc.setText(arraySanBong.get(position).Dc);
        ImageView imghinh = (ImageView) convertView.findViewById(R.id.ivhinh);
        imghinh.setImageResource(arraySanBong.get(position).Hinh);


        return convertView;
    }
}
