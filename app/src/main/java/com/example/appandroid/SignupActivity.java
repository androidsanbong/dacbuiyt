package com.example.appandroid;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SignupActivity extends AppCompatActivity {

    DatabaseHelper db;
    EditText e1,e2,e3,e4,e5;
    Button b1;
    TextView txt;
    public static final String EXTRA_USER="com.example.application.example.EXTRA_USER";
    public static final String EXTRA_PASS="com.example.application.example.EXTRA_PASS";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        db = new DatabaseHelper(this);
        e1=(EditText)findViewById(R.id.user);
        e2=(EditText)findViewById(R.id.pass);
        e3=(EditText)findViewById(R.id.rpass);
        b1=(Button)findViewById(R.id.dangki);
        txt=(TextView)findViewById(R.id.lg);
        txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SignupActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        });

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s1 = e1.getText().toString();
                String s2 = e2.getText().toString();
                String s3 = e3.getText().toString();
                if (s1.equals("")||s2.equals("")||s3.equals("")){
                    Toast.makeText(getApplicationContext(),"không được bỏ trống",Toast.LENGTH_SHORT).show();
                }
                else{
                    if (s2.equals(s3)){
                        Boolean checkuname = db.checkuname(s1);
                        if (checkuname==true){
                            Boolean insert = db.insert(s1,s2);
                            if (insert == true){
                                String user=e1.getText().toString();
                                String pass=e2.getText().toString();
                                Intent intent = new Intent(SignupActivity.this, LoginActivity.class);
                                intent.putExtra(EXTRA_USER,user);
                                intent.putExtra(EXTRA_PASS,pass);
                                startActivity(intent);
                                finish();
                            }
                        }
                        else {
                            Toast.makeText(getApplicationContext(),"Tên đăng nhập đã tồn tại",Toast.LENGTH_SHORT).show();
                        }

                    }else{
                        Toast.makeText(getApplicationContext(),"Mật khẩu không đúng",Toast.LENGTH_SHORT).show();
                        e3.setText("");
                        e2.setText("");
                    }
                }
            }
        });

    }
}
