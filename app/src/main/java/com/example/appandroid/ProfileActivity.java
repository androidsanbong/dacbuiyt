package com.example.appandroid;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ProfileActivity extends AppCompatActivity {
    TextView username;
    Button tc,dx;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        username = (TextView)findViewById(R.id.uname);
        tc =(Button)findViewById(R.id.home);
        dx =(Button)findViewById(R.id.logout);

        Intent intent = getIntent();
        String user = intent.getStringExtra(LoginActivity.EXTRA_USER);


        username.setText(user);

        tc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View s  ) {
                Intent intent = new Intent(ProfileActivity.this,ListviewhomeActivity.class);
                startActivity(intent);
            }
        });
        dx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View s  ) {
                Intent intent = new Intent(ProfileActivity.this,LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
