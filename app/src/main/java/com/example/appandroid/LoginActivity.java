package com.example.appandroid;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    EditText edtuser,edtpassword;
    Button btndangnhap;
    TextView txtdangki;
    DatabaseHelper db;
    TextView qmk;

    public static final String EXTRA_USER="com.example.application.example.EXTRA_USER";
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //get user and pass signup in SignupActivity
        Intent  intent = getIntent();
        String user = intent.getStringExtra(SignupActivity.EXTRA_USER);
        String pass = intent.getStringExtra(SignupActivity.EXTRA_PASS);

        db = new DatabaseHelper(this);
        edtuser = (EditText)findViewById(R.id.loginid);
        edtpassword = (EditText)findViewById(R.id.loginpass);
        btndangnhap = (Button)findViewById(R.id.btnlogin);
        txtdangki = (TextView)findViewById(R.id.txtsignin);
        edtuser.setText(user);
        edtpassword.setText(pass);
        qmk = (TextView)findViewById(R.id.txtqmk);


        btndangnhap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uname = edtuser.getText().toString();
                String password = edtpassword.getText().toString();
                Boolean checkunamepass = db.unamepassword(uname,password);
                if (checkunamepass==true) {
                    Intent intent = new Intent(LoginActivity.this, ProfileActivity.class);
                    intent.putExtra(EXTRA_USER, uname);
                    startActivity(intent);
                    Toast.makeText(LoginActivity.this,"Đăng nhập thành công",Toast.LENGTH_SHORT).show();
                    finish();
                }
                else
                    Toast.makeText(getApplicationContext(),"Sai tài khoản hoặc mật khẩu",Toast.LENGTH_SHORT).show();
            }
        });
        txtdangki.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View s  ) {
                Intent intent = new Intent(LoginActivity.this,SignupActivity.class);
                startActivity(intent);
                finish();
            }
        });

        qmk.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(LoginActivity.this,QuenmkActivity.class);
                startActivity(intent1);
            }
        });
    }
}