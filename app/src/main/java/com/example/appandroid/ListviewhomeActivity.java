package com.example.appandroid;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class ListviewhomeActivity extends AppCompatActivity {

    ListView Lvhome;
    ArrayList<SanBong> arraySanBong;
    private DrawerLayout drawer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listviewhome);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.draw_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this,drawer,toolbar,R.string.home_close,R.string.home_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        Lvhome = (ListView)findViewById(R.id.lvhome);
        arraySanBong = new ArrayList<SanBong>();
        arraySanBong.add(new SanBong("Sân Đa Phước","675 Nguyễn Tất Thành",R.drawable.hearder1));
        arraySanBong.add(new SanBong("Sân Chuyên Việt","675 Nguyễn Tất Thành",R.drawable.hearder1));
        arraySanBong.add(new SanBong("Sân Duy Tân","675 Nguyễn Tất Thành",R.drawable.hearder1));
        arraySanBong.add(new SanBong("Sân Đa Phước","675 Nguyễn Tất Thành",R.drawable.hearder1));
        arraySanBong.add(new SanBong("Sân Đa Phước","675 Nguyễn Tất Thành",R.drawable.hearder1));
        arraySanBong.add(new SanBong("Sân Đa Phước","675 Nguyễn Tất Thành",R.drawable.hearder1));
        arraySanBong.add(new SanBong("Sân Đa Phước","675 Nguyễn Tất Thành",R.drawable.hearder1));
        arraySanBong.add(new SanBong("Sân Đa Phước","675 Nguyễn Tất Thành",R.drawable.hearder1));
        arraySanBong.add(new SanBong("Sân Đa Phước","675 Nguyễn Tất Thành",R.drawable.hearder1));
        arraySanBong.add(new SanBong("Sân Đa Phước","675 Nguyễn Tất Thành",R.drawable.hearder1));
        arraySanBong.add(new SanBong("Sân Đa Phước","675 Nguyễn Tất Thành",R.drawable.hearder1));
        arraySanBong.add(new SanBong("Sân Đa Phước","675 Nguyễn Tất Thành",R.drawable.hearder1));
        arraySanBong.add(new SanBong("Sân Đa Phước","675 Nguyễn Tất Thành",R.drawable.hearder1));
        arraySanBong.add(new SanBong("Sân Đa Phước","675 Nguyễn Tất Thành",R.drawable.hearder1));
        arraySanBong.add(new SanBong("Sân Đa Phước","675 Nguyễn Tất Thành",R.drawable.hearder1));
        arraySanBong.add(new SanBong("Sân Đa Phước","675 Nguyễn Tất Thành",R.drawable.hearder1));
        arraySanBong.add(new SanBong("Sân Đa Phước","675 Nguyễn Tất Thành",R.drawable.hearder1));
        arraySanBong.add(new SanBong("Sân Đa Phước","675 Nguyễn Tất Thành",R.drawable.hearder1));
        arraySanBong.add(new SanBong("Sân Đa Phước","675 Nguyễn Tất Thành",R.drawable.hearder1));

        SanBongAdapter bongAdapter = new SanBongAdapter(
                ListviewhomeActivity.this,
                R.layout.activity_sanbong,
                arraySanBong
        );

        Lvhome.setAdapter(bongAdapter);

        Lvhome.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position==0){
                    Toast.makeText(ListviewhomeActivity.this,"Sân Đa Phước",Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(ListviewhomeActivity.this,DaphuocActivity.class);
                    startActivity(i);
                }
                if (position==1) {
                    Toast.makeText(ListviewhomeActivity.this, "Sân Chuyên Việt", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(ListviewhomeActivity.this, Layout1Activity.class);
                    startActivity(i);
                }
                if (position==2){
                    Toast.makeText(ListviewhomeActivity.this,"Sân Duy Tân",Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(ListviewhomeActivity.this,Layout7Activity.class);
                    startActivity(i);
                }
                if (position==3){
                    Toast.makeText(ListviewhomeActivity.this,"Sân Tuyên Sơn",Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(ListviewhomeActivity.this,Layout2Activity.class);
                    startActivity(i);
                }
                if (position==4){
                    Toast.makeText(ListviewhomeActivity.this,"Sân Trưng Vương",Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(ListviewhomeActivity.this,Layout3Activity.class);
                    startActivity(i);
                }
                if (position==5){

                    Toast.makeText(ListviewhomeActivity.this,"Sân Hưng yên",Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(ListviewhomeActivity.this,Layout4Activity.class);
                    startActivity(i);
                }
                if (position==6){
                    Toast.makeText(ListviewhomeActivity.this,"Sân Trần Cao vân",Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(ListviewhomeActivity.this,Layout5Activity.class);
                    startActivity(i);
                }
                if (position==7){
                    Toast.makeText(ListviewhomeActivity.this,"Sân Tuyên Sơn",Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(ListviewhomeActivity.this,Layout6Activity.class);
                    startActivity(i);
                }
                if (position==8){
                    Toast.makeText(ListviewhomeActivity.this,"Sân Đa Phước",Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(ListviewhomeActivity.this,Layout7Activity.class);
                    startActivity(i);
                }
                if (position==9){
                    Toast.makeText(ListviewhomeActivity.this,"Sân TDTT",Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(ListviewhomeActivity.this,Layout7Activity.class);
                    startActivity(i);
                }
            }
        });
    }
    @Override
    public void onBackPressed(){
        if (drawer.isDrawerOpen(GravityCompat.START))
        {
            drawer.closeDrawer(GravityCompat.START);
        }
        else{
            super.onBackPressed();
        }
    }
}
